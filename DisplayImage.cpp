#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace cv;

class generateBinaryImage {

    Mat imageGrey, imageColour, thresholdImage, binImage;

  public: 

    generateBinaryImage(Mat imageColour_, Mat imageGrey_) {
        imageColour = imageColour_;
        imageGrey = imageGrey_;
        binImage = createImageMask();
        generateThresholdImage();

    }

    Scalar getPixelColour(Mat image_, int x, int y) {
      return Scalar(image_.at<Vec3b>(x,y)[0], 
                    image_.at<Vec3b>(x,y)[1], 
                    image_.at<Vec3b>(x,y)[2]); 
    }

    Mat createImageMask(){
      return Mat::zeros( imageColour.size(), imageColour.type() );
    }

    void  generateThresholdImage() {
      //this lets us gage the brightness of each pixel, it will render the image as black and white only
       threshold( imageGrey, thresholdImage, 100, 255,CV_THRESH_BINARY | CV_THRESH_OTSU );
    }

    void displayImage() {

      namedWindow( "Binary image", CV_WINDOW_NORMAL );
      imshow( "Binary image", binImage);
      waitKey(0);
    }

    void generate() {

        string binaryNum;

        //iterate through every 30th pixel
        int x=0;
        while( x < imageColour.cols){ 
          int y = 0;
          while (y < imageColour.rows){

              // if its white
              if (getPixelColour(thresholdImage, y,x) == Scalar(255,255,255)){
                binaryNum = "0";  
              } else {
                binaryNum = "1";
              }

              putText( binImage, binaryNum, cvPoint(x,y), CV_FONT_HERSHEY_PLAIN,
                   3, getPixelColour(imageColour, y,x),2); 
              y= y+ 30; 
          }
          x= x+30;
      }

      displayImage();
    }



};


int main( int argc, char** argv ) {
  
  Mat imageGreyP = imread( argv[1], CV_LOAD_IMAGE_GRAYSCALE );
  Mat imageColourP = imread( argv[1], CV_LOAD_IMAGE_COLOR );

  if( argc != 2 || !imageGreyP.data || !imageColourP.data ) {
      printf( "No image data \n" );
      return -1;
  }

  generateBinaryImage* generator = new generateBinaryImage(imageColourP, imageGreyP);
  generator->generate();

  return 0;
}

